from django.urls import path
from cassette.views import HomePage, StationsListView

urlpatterns = [
    path('', HomePage.as_view(), name='index'),

    # REST API
    path('getStations', StationsListView.as_view()),
]
